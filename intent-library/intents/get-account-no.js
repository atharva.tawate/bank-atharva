"use strict";

module.exports = async (df) =>{
    df.setSynthesizeSpeech("I would like to verify the last 4 digits of your SSN")
    df.setResponseText("Can you tell me the last 4 digits of your SSN number?");
};
