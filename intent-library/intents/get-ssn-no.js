"use strict";

const firebase = require("../../config/firebase-config");

module.exports = async (df) =>{
    let outputContext = df.getContext("global");
    let parameters = outputContext.parameters;
    let accountNo = getDigits(parameters.account_no);
    let ssnNo = getDigits(parameters.ssn_no);
    const db = firebase.firestore();
    await db.collection("accont").where("account_no", "==", accountNo).
        where("ssn_no", "==", ssnNo).get().then(snapShot => {
            let account = snapShot.docs[0].data();
            console.log(account.amount);
            df.setResponseText(`Thank you for confirming! You currently have $${account.amount} in your account.`);
        }).catch(err => {
            console.log(err);
        })
};

const getDigits = (str) => {
    return parseInt(str.replace(/[^0-9]/gi, ''))
}
